from django.urls import path, include

from . import views

urlpatterns = [
    path("tasks/", include("tasks.urls")),
    path("", views.list_projects, name="list_projects"),
    path("create/", views.create_project, name="create_project"),
    path("<int:id>/", views.show_project, name="show_project"),
    path("", views.ProjectListView.as_view(), name="list_projects"),
]
