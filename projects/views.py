from django.shortcuts import render, get_object_or_404
from .models import Project
from django.shortcuts import redirect

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView

from django.contrib.auth.decorators import login_required
from . import forms


@login_required
def create_project(request):
    if request.method == "POST":
        form = forms.ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = forms.ProjectForm()
    return render(request, "projects/create_project.html", {"form": form})


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = project.tasks.all()
    return render(
        request,
        "projects/show_project.html",
        {"project": project, "tasks": tasks},
    )


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/list_projects.html", context)


def home(request):
    return redirect("list_projects")


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list_projects.html"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)
