from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import CreateTaskForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView
from .models import Task


class ShowMyTasksView(LoginRequiredMixin, ListView):
    template_name = "tasks/my_tasks.html"
    context_object_name = "tasks"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTaskForm()
    return render(request, "tasks/create_task.html", {"form": form})
